<?php
// reply to address
// get hostel info
$sql_smtp = "SELECT * FROM hostels WHERE id = '$hostel'";
   
$q_smtp = $db_object->query($sql_smtp);
if(DB::IsError($q_smtp)) {
   die($q_smtp->getMessage());
}

$result_smtp = $q_smtp->fetchRow(DB_FETCHMODE_OBJECT);

// account info for smtp
$mail->From = $result_smtp->email;
$mail->FromName = $result_smtp->name;

// set proprity (1=urgent, 3=normal and 5=low)
$mail->Priority = 3;

// get install
$sql = "SELECT * FROM preferences";
$q = $db_object->query($sql);
if(DB::IsError($q)) {
 die($q->getMessage());
}

$preferences = $q->fetchRow(DB_FETCHMODE_OBJECT);

$install = $preferences->install;

// include SMTP config file
include ("../includes/config/smtp_config_".$install.".php");
?>