<?php
    global $subject_privado, $body, $email_to, $namem, $var_control, $resultado;

    if (!isset($var_control)) die("hacking Attempt!");
    if ($var_control != "pandero39") die("hacking Attempt!");    

    if ($ControlErrores = false){
        error_reporting(E_ALL);
        ini_set('display_errors','On');
    } 

    // #############################################################################
    // ######################## Comienzo a preparar el mail ########################
    // #############################################################################

    require_once("mail/PHPMailerAutoload.php");
    $mail = new PHPMailer();

    // include language files
    include ("mail/smtp_admin.php");
            
    $mail->Subject    = $subject_privado;
    $mail->AltBody    = $body;
    $mail->MsgHTML( $body );
                
    // send copy to customer
    foreach ($mail_fromEmail as $cuenta) {
        $mail->AddAddress($cuenta, $mail_fromName);
    }
    

    // $mail->AddBCC(trim($valor),$result_smtp->name);

    $errortext ="";
    if(!$mail->Send()) {
        $errortext = "<br /><br />".$mail->ErrorInfo."<br /><br />".$email." ".$name."<br /><br />".$body;
        $resultado = false; //echo "No enviado!". $errortext; 
    } else {
        $resultado = true; //echo "Enviado!"; 
    }
?>