<?php
    // My modifications to mailer script from:
    // http://blog.teamtreehouse.com/create-ajax-contact-form
    // Added input sanitizing to prevent injection

    // Only process POST reqeusts.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Get the form fields and remove whitespace.
        $name = strip_tags(trim($_POST["name"]));
				$name = str_replace(array("\r","\n"),array(" "," "),$name);
        $email = filter_var(trim($_POST["email"]), FILTER_SANITIZE_EMAIL);
        $message = trim($_POST["message"]);

        // Check that data was sent to the mailer.
        if ( empty($name) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Set a 400 (bad request) response code and exit.
            http_response_code(400);
            echo "Oops! There was a problem with your submission. Please complete the form and try again.";
            exit;
        }

        if (isset($_REQUEST["type"])) {
            // expansion
            // Set the recipient email address.
            // FIXME: Update this to your desired email address.
            $recipient = "web@gruprv.es, expansion@gruprv.es";
            // Set the email subject.
            $subject = "Contacte expansió gruprv.es";
        } else {
            // otros
            // Set the recipient email address.
            // FIXME: Update this to your desired email address.
            $recipient = "web@gruprv.es, info@gruprv.es";
            // Set the email subject.
            $subject = "Contacte formulari gruprv.es";
        }
        
        // Build the email content.
        $email_content = "Nom: $name\n";
        $email_content .= "Email: $email\n\n";
        $email_content .= "Missatge:\n$message\n";

        // Build the email headers.
        $email_headers = "De: $name <$email>";

        // Send the email.
        if (mail($recipient, $subject, $email_content, $email_headers)) {
            // Set a 200 (okay) response code.
            http_response_code(200);
            echo "Gràcies per contactar amb nosaltres!";
        } else {
            // Set a 500 (internal server error) response code.
            http_response_code(500);
            echo "Oops! Per alguna raó no hem pogut rebre el teu missatge.";
        }

    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "Hi ha hagut algún problema. Si us plau, torna a intentar-ho.";
    }

?>
